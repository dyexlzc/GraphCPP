#include <iostream>
#include <cstdio>
#include "graph.h"
#include "undigraph.h"
using namespace std;
void digraph()
{
    Graph g("graph");
    cout << "build success" << endl;
    cout << "Vertex:" << g.V() << endl;
    for (int i = 0; i < g.V(); i++)
    {
        printf("[%d] in:", i);
        Graph::Sidetable *p = g.adj(i).firstin;
        while (p)
        {
            cout << p->from << " ";
            p = p->taillink;
        }

        printf("[%d] out:", i);
        p = g.adj(i).firstout;
        while (p)
        {
            cout << p->to << " ";
            p = p->headlink;
        }
        cout << endl;
    }
}
void undigraph(){
    Undigraph g("undigraph");
    cout<<"Undigraph"<<endl;
    for(int i=0;i<g.V;i++){
        printf("[%d] edges:",i);
        Undigraph::Sidetable *p=g._adj[i].firstedge;
        while(p){
            printf("%d-%d[%d]\t",p->ivex,p->jvex,p->weight);
            if(p->ivex==i)p=p->ilink;
            else p=p->jlink;
        }
        cout<<endl;
    }
    g.dijkstra(0,5);
}
int main()
{
    digraph();//建立并显示有向图
    cout<<"--------------"<<endl;
    undigraph();//建立并且显示无向图
    
    return 0;
}