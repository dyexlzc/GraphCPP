#pragma once
#include <vector>
#include <string>
using namespace std;

class Graph
{ //有向图
public:
    struct Sidetable
    { //边表
        int from, to;
        Sidetable *headlink, *taillink;
        Sidetable(int f, int t) : from(f), to(t)
        {
            headlink = nullptr;
            taillink = nullptr;
        }
    };
    struct Vertextable
    { //顶点表
        int data;
        Sidetable *firstin, *firstout;
        Vertextable()
        {
            firstin = nullptr;
            firstout = nullptr;
        }
    };
    int _V, _E;        //顶点&变
    Vertextable *_adj; //接邻表

    Graph(int v);
    Graph(const string &filename);
    int V() const;
    int E() const;
    Vertextable adj(int v); //和v相邻的所有顶点
    Vertextable *adj();     //单独获取接邻表
    string toString();      //字符串表示
};