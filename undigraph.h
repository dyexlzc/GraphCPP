#pragma once
#include <vector>
#include <string>
#include <string>
using namespace std;

class Undigraph
{ //无向图
public:
    struct Sidetable
    {               //边表
        int weight; //权重
        int ivex, jvex;
        Sidetable *ilink, *jlink;
        Sidetable(int iv, int jv) : ivex(iv), jvex(jv)
        {
            ilink = nullptr;
            jlink = nullptr;
        }
    };
    struct Vertextable
    { //顶点表
        int data;
        Sidetable *firstedge;
        Vertextable()
        {
            firstedge = nullptr;
        }
    };
    
    class point //运算符重载<
    {
    public:
        string local;
        int Vertex, Weight;
        point() {}
        point(int V, int W)
        {
            Vertex = V;
            Weight = W;
        }
    };
    struct cmp
    {
        bool operator()(point *&a, point *&b) const
        {
            return a->Weight > b->Weight;
        }
    } ;
    int V, E;
    Vertextable *_adj;
    Undigraph(const string &filename);
    vector<int> dijkstra(int source, int target);
};